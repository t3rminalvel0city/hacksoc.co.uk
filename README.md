**Intro**

This is the svn for the new and (hopefully) improved EHS website. The new site will be held under the domain www.hacksoc.co.uk as opposed to the very long ethicahackingsociety.com domain. 

**Whats Happening?**

The website will be developed in PHP/HTML/CSS (the usual).

**Whats In It For Me?**

You get that warm fuzzy feeling from helping out your society. And if that isn't enough I have an unspecified number of amazon gift-codes to give out to people who help out and do cool stuffs. *the ammount of said codes will vary based on the work done*