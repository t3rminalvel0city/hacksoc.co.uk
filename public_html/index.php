<?php
	include_once('includes/header.php');
?>

		<div class="container">

		<!-- Masthead
		================================================== -->
		<header class="jumbotron subhead" id="overview">
			<div class="row">
				<div class="span16">
					<h1>Ethical Hacking Society</h1>
					<p class="lead">We hack what we must, because, we can.</p>
				</div>		
			</div>
		</header>


		 <div class="content">


		<!-- Content
		================================================== -->
		<section data-spy="scroll" data-target=".subnav" id="welcome">
			<div class="page-header">
				<h1>Welcome!</h1>
			</div>

			<!-- Welcome shizzle -->
			<div class="row">

				<div class="span4">
					<br />
					<a class="twitter-timeline" href="https://twitter.com/AbertayHackers" data-widget-id="294559500017467393">Tweets by @AbertayHackers</a>
					<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
				</div>

				<div class="span8">
					<h3>Hello There!</h3>
					<p>Abertay Ethical Hacking Society is a group of like minded individuals from the University of Abertay Dundee and members of the public.</p>
					<p>Our meetings cover everything from Exploit development to Hardware hacking as well as talks from industry.</p>
					<p>Along side our meetings we have regular outings to other conferences and social events.</p>
					<p>We meet every wednesday during term time in room 3522 at 7 o'clock followed by a visit to the local watering hole to discuss the meeting and chat.</p>
					<p>This year we are planning on having team projects which can be nearly anything related to hacking. This will allow for the more experienced members to pass on knowledge to those who may be new to hacking while also having a good time doing something fun!</p>
					<p>Ethical hacking society members run the Securi-Tay InfoSec conference which has proved a big success and aided lots of them getting jobs and industry contacts. We are currently planning Securi-Tay3.</p>
					<p>You can view our upcoming talks and previous ones on the talks page, The talks page also allows you to see the abstracts of the talk and where available a video of the talk alongside slides and other resources.</p>
				</div>
			</div>
		</section>

		<?php

			foreach($db->query("SELECT author, DATE_FORMAT(talkdate, '%e/%c/%Y') AS talkdate, talktitle, abstract FROM talks WHERE talkdate > NOW() LIMIT 0,1") as $row) {
			    $author = $row['author'];
			    $talkdate = $row['talkdate'];
			    $talktitle = $row['talktitle'];
			    $abstract = $row['abstract'];
			}

		?>
			<!-- Welcome shizzle -->
			<div class="row">
				<div class="span16">
					<h3>Next Talk: <?php echo $talkdate . " - " . $talktitle; ?></h3>
				</div>
			</div>
			<div class="row">
				<div class="span4"></div>
				<div class="span8">
					<p><?php echo $abstract; ?></p>
				</div>
			</div>

<?php
	include_once('includes/footer.php')
?>
