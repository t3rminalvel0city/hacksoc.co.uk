<?php
	include_once('includes/header.php');
?>

		<div class="container">

		<!-- Masthead
		================================================== -->
		<header class="jumbotron subhead" id="overview">
			<div class="row">
				<div class="span16">
					<h1>Ethical Hacking Society</h1>
					<p class="lead">We hack what we must, because, we can.</p>
				</div>		
			</div>
		</header>


		 <div class="content">


		<!-- Content
		================================================== -->
		<section data-spy="scroll" data-target=".subnav" id="welcome">
			<div class="page-header">
				<h1>Talks</h1>
			</div>

			<!-- Talk Loop -->
			<div class="row">
				<div class="span4">
						<a class="twitter-timeline" href="https://twitter.com/AbertayHackers" data-widget-id="294559500017467393">Tweets by @AbertayHackers</a>
						<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
				</div>
				<div class="span8">
					<h3>Hello There!</h3>
					<p>Abertay Ethical Hacking Society is a group of like minded individuals from the University of Abertay Dundee and members of the public.</p>
					<p>Our meetings cover everything from Exploit development to Hardware hacking as well as talks from industry.</p>
					<p>Along side our meetings we have regular outings to other conferences and social events.</p>
					<p>We meet every wednesday during term time in room 3522 at 7 o'clock followed by a visit to the local watering hole to discuss the meeting and chat.</p>
					<p>You can view our upcoming talks and previous ones on the talks page, The talks page also allows you to see the abstracts of the talk and where available a video of the talk alongside slides and other resources.</p>
				</div>
			</div>

		</section>

<?php
	include_once('includes/footer.php')
?>
