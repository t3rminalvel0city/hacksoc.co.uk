		<!-- Footer
		================================================== -->
		<hr>
			<footer id="footer">
			<p class="pull-right"><a href="#">Back to top</a></p>
			<div class="links">
				<a target="_blank" href="https://twitter.com/abertayhackers">Twitter</a>					
				<a target="_blank" href="https://www.facebook.com/AbertayHackers">Facebook</a>
				<a target="_blank" href="https://github.com/AbertayHackers">GitHub</a>
			</footer>

			</div>
		</div><!-- /container -->

		<!-- Le javascript
		================================================== -->
		<!-- Placed at the end of the document so the pages load faster -->
		<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
		<script src="js/bootstrap.min.js"></script>
		<script src="js/application.js"></script>
		<script src="js/bootswatch.js"></script>

	</body>
</html>
