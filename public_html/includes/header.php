<?php	
	include_once('config/connect.php');
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<title>hacksoc.co.uk</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="">
		<meta name="author" content="">

		<!--[if lt IE 9]>
			<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->

		<link href="css/bootstrap.css" rel="stylesheet">
		<link href="css/font-awesome.css" rel="stylesheet">
		<link href="css/bootstrap-responsive.min.css" rel="stylesheet">
		<link href="css/bootswatch.css" rel="stylesheet">

	</head>

<body class="preview" data-spy="scroll" data-target=".subnav" data-offset="80">

	
	<!-- Login Modal -->
	<div id="loginModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	  <div class="modal-header">
	    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
	    <h3 id="myModalLabel">Login</h3>
	  </div>
	  <div class="modal-body">
	    <p>Form go here</p>
	  </div>
	  <div class="modal-footer">
	    <button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
	    <button class="btn btn-primary">Login</button>
	  </div>
	</div>

		<!-- Reset Modal -->
	<div id="resetModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	  <div class="modal-header">
	    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
	    <h3 id="myModalLabel">Reset Password</h3>
	  </div>
	  <div class="modal-body">
	    <p>Form go here</p>
	  </div>
	  <div class="modal-footer">
	    <button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
	    <button class="btn btn-primary">Reset</button>
	  </div>
	</div>

		<!-- Register Modal -->
	<div id="registerModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	  <div class="modal-header">
	    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
	    <h3 id="myModalLabel">Register</h3>
	  </div>
	  <div class="modal-body">
	    <p>Form go here</p>
	  </div>
	  <div class="modal-footer">
	    <button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
	    <button class="btn btn-primary">Register</button>
	  </div>
	</div>

	<!-- Navbar
		================================================== -->
 <div class="navbar navbar-fixed-top">
	 <div class="navbar-inner">
		 <div class="container">
			 <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
				 <span class="icon-bar"></span>
				 <span class="icon-bar"></span>
				 <span class="icon-bar"></span>
			 </a>
			 <a class="brand" href="index.php">EHS</a>
			 <div class="nav-collapse" id="main-menu">
				<ul class="nav" id="main-menu-left">
					<li><a href="talks.php">Talks</a></li>
					<li class="dropdown">
						<a class="dropdown-toggle" data-toggle="dropdown" href="#">Resources <b class="caret"></b></a>
						<ul class="dropdown-menu" id="swatch-menu">
							<li><a target="_blank" href="https://docs.google.com/a/ethicalhackingsociety.com/spreadsheet/ccc?key=0AizI1vTtD_ugdEN4OVU4X0dJSC1Hb1lfMzNGd00wb3c#gid=0"><i class="icon-money"></i> Society Accounts</a></li>
						</ul>
					</li>
				</ul>
				<ul class="nav pull-right" id="main-menu-right">
					<li class="dropdown">
						<a class="dropdown-toggle" data-toggle="dropdown" href="#">Account <b class="caret"></b></a>
						<ul class="dropdown-menu" id="swatch-menu">
							<li><a href="#loginModal" data-toggle="modal"><i class="icon-signin"></i> Login</a></li>
							<li><a href="#resetModal" data-toggle="modal"><i class="icon-question-sign"></i> Reset Password</a></li>
							<li class="divider"></li>
							<li><a href="#registerModal" data-toggle="modal"><i class="icon-pencil"></i> Register</a></li>
							<li><a href="#registerModal" data-toggle="modal"><i class="icon-edit"></i> Admin Panel</a></li>
						</ul>
					</li>
				</ul>
			 </div>
		 </div>
	 </div>
 </div>